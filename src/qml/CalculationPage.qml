/*
 * SPDX-FileCopyrightText: 2020-2021 Han Young <hanyoung@protonmail.com>
 * SPDX-FileCopyrightText: 2020 Devin Lin <espidev@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1 as Controls
import QtGraphicalEffects 1.12
import org.kde.kirigami 2.13 as Kirigami

Kirigami.Page {
    id: initialPage
    title: i18n("Calculator")
    topPadding: 0
    leftPadding: 0
    rightPadding: 0
    bottomPadding: 0
    
    property color dropShadowColor: Qt.darker(Kirigami.Theme.backgroundColor, 1.15)
    property int keypadHeight: {
        let rows = 4, columns = 3;
        // restrict keypad so that the height of buttons never go past 0.85 times their width
        if ((initialPage.height - Kirigami.Units.gridUnit * 7) / rows > 0.85 * initialPage.width / columns) {
            return rows * 0.85 * initialPage.width / columns;
        } else {
            return initialPage.height - Kirigami.Units.gridUnit * 7;
        }
    }
    
    Keys.onPressed: {
        switch(event.key) {
        case Qt.Backspace || Qt.Delete:
            inputManager.backspace(); break;
        case Qt.Key_0:
            inputManager.append("0"); break;
        case Qt.Key_1:
            inputManager.append("1"); break;
        case Qt.Key_2:
            inputManager.append("2"); break;
        case Qt.Key_3:
            inputManager.append("3"); break;
        case Qt.Key_4:
            inputManager.append("4"); break;
        case Qt.Key_5:
            inputManager.append("5"); break;
        case Qt.Key_6:
            inputManager.append("6"); break;
        case Qt.Key_7:
            inputManager.append("7"); break;
        case Qt.Key_8:
            inputManager.append("8"); break;
        case Qt.Key_9:
            inputManager.append("9"); break;
        case Qt.Key_Plus:
            inputManager.append("+"); break;
        case Qt.Key_Minus:
            inputManager.append("-"); break;
        case Qt.Key_multiply:
            inputManager.append("×"); break;
        case Qt.Key_division:
            inputManager.append("÷"); break;
        case Qt.Key_AsciiCircum:
            inputManager.append("^"); break;
        case Qt.Key_Period:
            inputManager.append("."); break;
        case Qt.Key_Equal:
        case Qt.Key_Return:
        case Qt.Key_Enter:
            inputManager.equal(); break;
        }
    }
    
    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        
        Rectangle {
            id: outputScreen
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            Layout.preferredHeight: initialPage.height - initialPage.keypadHeight
            color: Kirigami.Theme.backgroundColor
            
            Column {
                id: outputColumn
                anchors.fill: parent
                anchors.margins: Kirigami.Units.largeSpacing
                spacing: Kirigami.Units.gridUnit
                
                Flickable {
                    anchors.right: parent.right
                    height: Kirigami.Units.gridUnit * 1.5
                    width: Math.min(parent.width, contentWidth)
                    contentHeight: expressionRow.height
                    contentWidth: expressionRow.width
                    flickableDirection: Flickable.HorizontalFlick
                    Controls.Label {
                        id: expressionRow
                        horizontalAlignment: Text.AlignRight
                        font.pointSize: Kirigami.Units.gridUnit
                        text: inputManager.expression
                        color: Kirigami.Theme.disabledTextColor
                    }
                    onContentWidthChanged: {
                        if(contentWidth > width)
                            contentX = contentWidth - width;
                    }
                }
                
                Flickable {
                    anchors.right: parent.right
                    height: Kirigami.Units.gridUnit * 4
                    width: Math.min(parent.width, contentWidth)
                    contentHeight: result.height
                    contentWidth: result.width
                    flickableDirection: Flickable.HorizontalFlick
                    Controls.Label {
                        id: result
                        horizontalAlignment: Text.AlignRight
                        font.pointSize: Kirigami.Units.gridUnit * 2
                        text: inputManager.result
                        NumberAnimation on opacity {
                            id: resultFadeInAnimation
                            from: 0.5
                            to: 1
                            duration: Kirigami.Units.shortDuration
                        }
                        NumberAnimation on opacity {
                            id: resultFadeOutAnimation
                            from: 1
                            to: 0
                            duration: Kirigami.Units.shortDuration
                        }

                        onTextChanged: resultFadeInAnimation.start()
                    }
                }
            }
        }
        
        // keypad area
        Rectangle {
            property string expression: ""
            id: inputPad
            Layout.fillHeight: true
            Layout.preferredWidth: initialPage.width
            Layout.alignment: Qt.AlignLeft
            Kirigami.Theme.colorSet: Kirigami.Theme.View
            Kirigami.Theme.inherit: false
            color: Kirigami.Theme.backgroundColor
            
            NumberPad {
                id: numberPad
                anchors.fill: parent
                anchors.topMargin: Kirigami.Units.gridUnit * 0.7
                anchors.bottomMargin: Kirigami.Units.smallSpacing
                anchors.leftMargin: Kirigami.Units.smallSpacing
                anchors.rightMargin: Kirigami.Units.gridUnit * 1.5 // for right side drawer indicator
                onPressed: {
                    if (text == "DEL") {
                        inputManager.backspace();
                    } else if (text == "=") {
                        inputManager.equal();
                        resultFadeOutAnimation.start();
                    } else {
                        inputManager.append(text);
                    }
                }
                onClear: inputManager.clear()
            }
            
            Rectangle {
                id: drawerIndicator
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: Kirigami.Units.gridUnit
                x: parent.width - this.width
                
                Kirigami.Theme.colorSet: Kirigami.Theme.View
                color: Kirigami.Theme.backgroundColor
                
                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: -2
                    verticalOffset: 0
                    radius: 4
                    samples: 6
                    color: initialPage.dropShadowColor
                }
                
                Rectangle {
                    anchors.centerIn: parent
                    height: parent.height / 20
                    width: parent.width / 4
                    radius: 3
                    color: Kirigami.Theme.textColor
                }
            }

            Controls.Drawer {
                id: functionDrawer
                parent: initialPage
                y: initialPage.height - inputPad.height
                height: inputPad.height
                width: initialPage.width * 0.6
                dragMargin: drawerIndicator.width
                edge: Qt.RightEdge
                dim: false
                onXChanged: drawerIndicator.x = this.x - drawerIndicator.width + drawerIndicator.radius
                opacity: 1 // for plasma style
                FunctionPad {
                    anchors.fill: parent
                    anchors.bottom: parent.Bottom
                    anchors.leftMargin: Kirigami.Units.largeSpacing
                    anchors.rightMargin: Kirigami.Units.largeSpacing
                    anchors.topMargin: Kirigami.Units.largeSpacing
                    anchors.bottomMargin: parent.height / 4
                    onPressed: inputManager.append(text)
                }
                // for plasma style
                background: Rectangle {
                    Kirigami.Theme.colorSet: Kirigami.Theme.View
                    color: Kirigami.Theme.backgroundColor
                    anchors.fill: parent
                }
            }
        }
        
        // top panel drop shadow (has to be above the keypad)
        DropShadow {
            anchors.fill: outputScreen
            source: outputScreen
            horizontalOffset: 0
            verticalOffset: 1
            radius: 4
            samples: 6
            color: initialPage.dropShadowColor
        }
    }
    
}
